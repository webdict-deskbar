# translation of deskbar-applet.HEAD.po to Georgian
# Georgian translation for Deskbar Applet.
# Copyright © 2006 Ubuntu Georgian Translators.
# This file is distributed under the same license as the deskbar-applet package.
# Gia Shervashidze <giasher@telenet.ge>., 2006.
# Vladimer Sichinava <vsichi@gnome.org>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: deskbar-applet.HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-09-26 01:45+0200\n"
"PO-Revision-Date: 2006-09-26 02:54+0200\n"
"Last-Translator: Vladimer Sichinava <vlsichinava@gmail.com>\n"
"Language-Team: Georgian <www.gia.ge>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0"

#: ../data/Deskbar_Applet.server.in.in.h:1
msgid "An all-in-one action bar"
msgstr "ყველა-ერთში ქმედების პანელი"

#: ../data/Deskbar_Applet.server.in.in.h:2 ../deskbar/ui/About.py:21
msgid "Deskbar"
msgstr "Deskbar"

#: ../data/Deskbar_Applet.xml.h:1 ../deskbar/ui/window/CuemiacWindowUI.py:56
msgid "_About"
msgstr "_შესახებ"

#: ../data/Deskbar_Applet.xml.h:2 ../deskbar/ui/window/CuemiacWindowUI.py:52
msgid "_Clear History"
msgstr "გ_აწმინდე ისტორია"

#: ../data/Deskbar_Applet.xml.h:3 ../deskbar/ui/window/CuemiacWindowUI.py:53
msgid "_Preferences"
msgstr "_პარამეტრები"

#: ../data/prefs-dialog.glade.h:1
msgid "<b>Focus</b>"
msgstr "<b>ფოკუსი</b>"

#: ../data/prefs-dialog.glade.h:2
msgid "<b>Layout</b>"
msgstr "<b>განთავსება</b>"

#: ../data/prefs-dialog.glade.h:3
msgid "<b>Width</b>"
msgstr "<b>სიგანე</b>"

#: ../data/prefs-dialog.glade.h:4
msgid ""
"<i><small><b>Note:</b> Drag and drop an extension to change its order.</"
"small></i>"
msgstr ""
"<i><small><b>შენიშვნა:</b> გადააადგილეთ პაკეტები მათი რიგის შესაცვლელად.</"
"small></i>"

#: ../data/prefs-dialog.glade.h:5
msgid "Button in panel"
msgstr "ღილაკი პანელზე"

#: ../data/prefs-dialog.glade.h:6
msgid "Deskbar Preferences"
msgstr "Deskbar პარამეტრები"

#: ../data/prefs-dialog.glade.h:7
msgid "Entry in panel"
msgstr "პანელის ელემენტი"

#: ../data/prefs-dialog.glade.h:8
msgid "Extensions"
msgstr "გაფართოებები"

#: ../data/prefs-dialog.glade.h:9
msgid "Fixed _width:"
msgstr "ფიქსირებული _სიგანე:"

#: ../data/prefs-dialog.glade.h:10
msgid "Search selection when triggering the shortcut"
msgstr "მონიშნულის ძიება მალხმობის გამოძახების შემთხვევაში"

#: ../data/prefs-dialog.glade.h:11
msgid "Use _all available space"
msgstr "_მისაწვდომი სივრცის სრულად გამოყენება"

#: ../data/prefs-dialog.glade.h:12
msgid "View"
msgstr "ჩვენება"

#: ../data/prefs-dialog.glade.h:13
msgid "_Keyboard shortcut to focus:"
msgstr "ფოკუსირების _კლავიშების კომბინაცია:"

#: ../data/prefs-dialog.glade.h:14
msgid "_More..."
msgstr "_დეტალები..."

#: ../data/prefs-dialog.glade.h:15
msgid "characters"
msgstr "ასო-ნიშნები"

#: ../data/smart-bookmarks.glade.h:1
msgid ""
"<i><small><b>Note: </b>If that shortcut is a single letter (like <b>t</b>) "
"you can also just type \"<b>something</b>\" and then press <b>Alt-t</b> in "
"the deskbar.</small></i>"
msgstr ""
"<i><small><b>შენიშვნა: </b>თუ მალმხმობი ერთი სიმბოლოა (მაგ. <b>t</b>) "
"შეგიძლიათ აკრიფოთ \"<b>რაიმე ტექსტი</b>\" და შემდეგ დააჭიროთ <b>Alt-t</b>-ს."
"</small></i>"

#: ../data/smart-bookmarks.glade.h:2
msgid ""
"<i><small><b>Note:</b> To use a shortcut (for example <b>wp</b>) to search "
"for <b>something</b>, type \"<b>wp something</b>\" in the deskbar</small></"
"i>."
msgstr ""
"<i><small><b>შენიშვნა:</b>მალმხმობის გამოსაყენებლად (მაგალითად <b>wp</b>) "
"რათა მოძებნოდ <b>something</b>, საჭიროა აკრიფოთ \"<b>wp რაიმე</b>\"</small></"
"i>."

#: ../data/smart-bookmarks.glade.h:3
msgid "Shortcuts for Bookmarked Searches"
msgstr "მალმხმობები ჩანიშნული ძიებებისთვის"

#: ../deskbar/BrowserMatch.py:46 ../deskbar/handlers/beagle-live.py:152
#, python-format
msgid "Open History Item %s"
msgstr "ისტორიის ელემენტის გახსნა - %s"

#: ../deskbar/BrowserMatch.py:48
#, python-format
msgid "Open Bookmark %s"
msgstr "სანიშნის გახსნა - %s"

#. translators: First %s is the search engine name, second %s is the search term
#: ../deskbar/BrowserMatch.py:97
#, python-format
msgid "Search <b>%(name)s</b> for <i>%(text)s</i>"
msgstr "ძიება: <b>%(name)s</b> - <i>%(text)s</i>"

#: ../deskbar/BrowserMatch.py:206
msgid "Shortcut"
msgstr "მალმხმობი"

#: ../deskbar/BrowserMatch.py:214
msgid "Bookmark Name"
msgstr "სანიშნის სახელი"

#: ../deskbar/Categories.py:7
msgid "Uncategorized"
msgstr "დაუჯგუფებელი"

#: ../deskbar/Categories.py:10 ../deskbar/handlers/history.py:9
msgid "History"
msgstr "ისტორია"

#: ../deskbar/Categories.py:15
msgid "Documents"
msgstr "დოკუმენტები"

#: ../deskbar/Categories.py:18
msgid "Emails"
msgstr "ელმისამართები"

#: ../deskbar/Categories.py:21
msgid "Conversations"
msgstr "საუბრები"

#: ../deskbar/Categories.py:24
msgid "Files"
msgstr "ფაილი"

#: ../deskbar/Categories.py:27
msgid "People"
msgstr "ხალხი"

#: ../deskbar/Categories.py:30
msgid "Places"
msgstr "ადგილი"

#: ../deskbar/Categories.py:33
msgid "Actions"
msgstr "ქმედება"

#: ../deskbar/Categories.py:36 ../deskbar/handlers/web_address.py:11
msgid "Web"
msgstr "ვები"

#: ../deskbar/Categories.py:39
msgid "Web Search"
msgstr "ძიება ქსელში"

#: ../deskbar/Categories.py:42
msgid "News"
msgstr "ახალი ამბები"

#: ../deskbar/Categories.py:45
msgid "Notes"
msgstr "შენიშვნები"

#: ../deskbar/DeskbarHistory.py:37
msgid "No History"
msgstr "ისტორიის გარეშე"

#: ../deskbar/gtkexcepthook.py:14
msgid "A programming error has been detected"
msgstr "დაიმზირა პროგრამის შეცდომა"

#: ../deskbar/gtkexcepthook.py:15
msgid ""
"It probably isn't fatal, but should be reported to the developers "
"nonetheless. The program may behave erratically from now on."
msgstr ""
"შესაძლოა არა კრიტიკული, მაგრამ მაინც საჭიროა პროგრამისტებისთვის შეტყობინება. "
"ამის შემდეგ პროგრამა შესაძლოა მუშაობა მცდარად გააგრძელოს."

#: ../deskbar/gtkexcepthook.py:17
msgid "Bug Detected"
msgstr "დაიმზირა შეცდომა"

#: ../deskbar/handlers/beagle-live.py:19
msgid "Start Beagle Daemon?"
msgstr "გავუშვა Beagle დემონი?"

#: ../deskbar/handlers/beagle-live.py:24
msgid "Start Beagle Daemon"
msgstr "Beagle დემონის გაშვება"

#: ../deskbar/handlers/beagle-live.py:25
msgid ""
"The Beagle daemon does not appear to be running.\n"
" You need to start it to use the Beagle Live handler."
msgstr ""
"ეტყობა რომ Beagle დემონი არ არის გაშვებული.\n"
" ჩართეთ იგი თუ Beagle-ს გამოყენება გსურთ."

#: ../deskbar/handlers/beagle-live.py:41
msgid "Failed to start Beagle"
msgstr "შეუძლებელია Beagle-ს გაშვება"

#: ../deskbar/handlers/beagle-live.py:42
msgid "Perhaps the beagle daemon isn't installed?"
msgstr "იქნებ Beagle-ს დემონი არ არის დაყენებული ?"

#: ../deskbar/handlers/beagle-live.py:65
msgid "Beagle Live"
msgstr "Beagle Live"

#: ../deskbar/handlers/beagle-live.py:66
msgid "Search all of your documents (using Beagle), as you type"
msgstr ""
"ძიება თქვენს ყველა დოკუმენტში (Beagle-ს გამოყენებით), ტექსტის შეტანისას"

#: ../deskbar/handlers/beagle-live.py:94
#, python-format
msgid "Edit contact %s"
msgstr "კონტაქტის რედაქტირება %s"

#: ../deskbar/handlers/beagle-live.py:103
#, python-format
msgid "From %s"
msgstr "ვისგან %s"

#. translators: This is a file.
#: ../deskbar/handlers/beagle-live.py:111 ../deskbar/handlers/files.py:46
#, python-format
msgid "Open %s"
msgstr "გახსნა - %s"

#: ../deskbar/handlers/beagle-live.py:119
#, python-format
msgid "News from %s"
msgstr "სიახლეთა მიღება - %s"

#: ../deskbar/handlers/beagle-live.py:128
#, python-format
msgid "Note: %s"
msgstr "შენიშვნა: %s"

#: ../deskbar/handlers/beagle-live.py:137
#, python-format
msgid "With %s"
msgstr "რითი %s"

#: ../deskbar/handlers/beagle-live.py:145
#, python-format
msgid "Calendar: %s"
msgstr "კალენდარი: %s"

#. translators: This is used for unknown values returned by beagle
#. translators: for example unknown email sender, or unknown note title
#: ../deskbar/handlers/beagle-live.py:321
#: ../deskbar/handlers/beagle-live.py:343
msgid "?"
msgstr "?"

#: ../deskbar/handlers/beagle-static.py:20
msgid "Beagle"
msgstr "Beagle"

#: ../deskbar/handlers/beagle-static.py:21
msgid "Search all of your documents (using Beagle)"
msgstr "ძიება თქვენს ყველა დოკუმენტში (Beagle-ს გამოყენებით)"

#: ../deskbar/handlers/beagle-static.py:38
#, python-format
msgid "Search for %s using Beagle"
msgstr "ძიება - %s (Beagle-ს გამოყენებით)"

#: ../deskbar/handlers/desklicious.py:16
msgid "You need to configure your del.icio.us account."
msgstr "საჭიროა თქვენი del.icio.us ანგარიშის კონფიგურირება."

#: ../deskbar/handlers/desklicious.py:18
msgid "You can modify your del.icio.us account."
msgstr "შეგიძლიათ თქვენი del.icio.us ანგარიშის შეცვლა."

#: ../deskbar/handlers/desklicious.py:22
msgid "del.icio.us Bookmarks"
msgstr "del.icio.us სანიშნეები"

#: ../deskbar/handlers/desklicious.py:23
msgid "Search your del.icio.us bookmarks by tag name"
msgstr "del.icio.us ვებ სანიშნეში ჭდის სახელის მიხედვით ძიება"

#: ../deskbar/handlers/desklicious.py:30
msgid "del.icio.us Account"
msgstr "del.icio.us ანგარიში"

#: ../deskbar/handlers/desklicious.py:37
msgid "Enter your del.icio.us username below"
msgstr "შეყვანეთ თქვენი del.icio.us მომხმარებლის სახელი"

#: ../deskbar/handlers/desklicious.py:43
msgid "Username: "
msgstr "მომხმარებლის სახელი:"

#: ../deskbar/handlers/epiphany.py:27
msgid "You can set shortcuts for your searches."
msgstr "ძიებისთვის მალმხმობების მითითება შეგიძლიათ"

#: ../deskbar/handlers/epiphany.py:33 ../deskbar/handlers/galeon.py:24
#: ../deskbar/handlers/mozilla.py:129
msgid "Web Bookmarks"
msgstr "ვებ სანიშნეები"

#: ../deskbar/handlers/epiphany.py:34 ../deskbar/handlers/galeon.py:25
#: ../deskbar/handlers/mozilla.py:130
msgid "Open your web bookmarks by name"
msgstr "ვებ სანიშნეების სახელის მიხედვით გახსნა"

#: ../deskbar/handlers/epiphany.py:39 ../deskbar/handlers/galeon.py:30
#: ../deskbar/handlers/mozilla.py:141
msgid "Web History"
msgstr "ვებ ისტორია"

#: ../deskbar/handlers/epiphany.py:40 ../deskbar/handlers/galeon.py:31
#: ../deskbar/handlers/mozilla.py:142
msgid "Open your web history by name"
msgstr "ვებ ისტორიის სახელის მიხედვით გახსნა"

#: ../deskbar/handlers/epiphany.py:45 ../deskbar/handlers/galeon.py:36
#: ../deskbar/handlers/mozilla.py:135
msgid "Web Searches"
msgstr "ვებ ძიებები"

#: ../deskbar/handlers/epiphany.py:46 ../deskbar/handlers/galeon.py:37
#: ../deskbar/handlers/mozilla.py:136
msgid "Search the web via your browser's search settings"
msgstr "ვებ ძიება ბრაუზერის საძიებო პარამეტრების მიხედვით"

#: ../deskbar/handlers/evolution.py:12
msgid "You need to enable autocomplete in your mail preferences"
msgstr "გესაჭიროებათ თვითდასრულების გააქტივება ფოსტის პარამეტრებში"

#: ../deskbar/handlers/evolution.py:15
msgid "Autocompletion Needs to be Enabled"
msgstr "თვითდასრულება უნდა გააქტივდეს"

#: ../deskbar/handlers/evolution.py:16
msgid ""
"We cannot provide e-mail addresses from your address book unless "
"autocompletion is enabled.  To do this, from your mail program's menu, "
"choose Edit - Preferences, and then Autocompletion."
msgstr ""
"ჩვენ ვერ მოგაწვდით ელფოსტის მისამართებს თქვენი წიგნაკიდან თუ თვითდასრულება "
"გამორთულია. ამისთვის, თქვენი საფოსტო პროგრამის მენიუში აირჩიეთ  რედაქტირება "
"- პარამეტრები და შემდეგ თვითდასრულება."

#: ../deskbar/handlers/evolution.py:21
msgid "Mail (Address Book)"
msgstr "ფოსტა (წიგნაკი)"

#: ../deskbar/handlers/evolution.py:22
msgid "Send mail to your contacts by typing their name or e-mail address"
msgstr ""
"ფოსტის გაგზავნა თქვენი კონტაქტებისთვის მათი სახელისა და ელფოსტის მითითებით"

#. translators: First %s is the contact full name, second %s is the email address
#: ../deskbar/handlers/evolution.py:48
#, python-format
msgid "Send Email to <b>%(name)s</b> (%(email)s)"
msgstr "ელფოსტის გაგზავნა - <b>%(name)s</b> (%(email)s)"

#: ../deskbar/handlers/files.py:17
msgid "Files, Folders and Places"
msgstr "ფაილები, საქაღალდეები და ადგილმდებარეობები"

#: ../deskbar/handlers/files.py:18
msgid "View your files, folders, bookmarks, drives, network places by name"
msgstr ""
"ფაილების, დასტების, სანიშნეების, მოწყობილობების სახელის მიხედვით დათვალიერება"

#: ../deskbar/handlers/files.py:68
#, python-format
msgid "Open folder %s"
msgstr "დასტის გახსნა - %s"

#: ../deskbar/handlers/files.py:88 ../deskbar/handlers/files.py:118
#, python-format
msgid "Open location %s"
msgstr "მისამართის გახსნა - %s"

#: ../deskbar/handlers/files.py:114
#, python-format
msgid "Open network place %s"
msgstr "ქსელის კვანძის გახასნა - %s"

#: ../deskbar/handlers/files.py:116
#, python-format
msgid "Open audio disc %s"
msgstr "აუდიო დისკის გახსნა %s"

#: ../deskbar/handlers/gdmactions.py:10
msgid "Computer Actions"
msgstr "კომპიუტერის ქმედებები"

#: ../deskbar/handlers/gdmactions.py:11
msgid "Logoff, shutdown, restart, suspend and related actions."
msgstr "გამოსვლა, გამორთვა, გადატვირთვა, შეჩერება და ამის მაგვარი ქმედებები."

#: ../deskbar/handlers/gdmactions.py:44
msgid "Suspend the machine"
msgstr "მანქანის შეჩერება"

#: ../deskbar/handlers/gdmactions.py:60
msgid "Hibernate the machine"
msgstr "მანქანის ჰიბერნაცია"

#: ../deskbar/handlers/gdmactions.py:76
msgid "Shutdown the machine"
msgstr "მანქანის გამორთვა"

#: ../deskbar/handlers/gdmactions.py:100
msgid "Lock the screen"
msgstr "ეკრანის დაბლოკვა"

#: ../deskbar/handlers/gdmactions.py:126
msgid "Shut Down"
msgstr "გამორთვა"

#: ../deskbar/handlers/gdmactions.py:133
msgid "Turn off the computer"
msgstr "კომპიუტერის გამორთვა"

#: ../deskbar/handlers/gdmactions.py:137
msgid "Log Out"
msgstr "გამოსვლა"

#: ../deskbar/handlers/gdmactions.py:144
msgid "Log out"
msgstr "გასვლა"

#: ../deskbar/handlers/gdmactions.py:148
msgid "Restart"
msgstr "გადატვირთე"

#: ../deskbar/handlers/gdmactions.py:155
msgid "Restart the computer"
msgstr "კომპიუტერის გადატვირთვა"

#: ../deskbar/handlers/gdmactions.py:159 ../deskbar/handlers/gdmactions.py:165
msgid "Switch User"
msgstr "მომხმარებლის გადართვა"

#: ../deskbar/handlers/gdmactions.py:185
msgid "Lock"
msgstr "დაბლოკვა"

#: ../deskbar/handlers/gdmactions.py:196
msgid "Suspend"
msgstr "შეჩერება"

#: ../deskbar/handlers/gdmactions.py:198
msgid "Hibernate"
msgstr "ჰიბერნაცია"

#: ../deskbar/handlers/gdmactions.py:200
msgid "Shutdown"
msgstr "გამორთვა"

#: ../deskbar/handlers/google-live.py:20
msgid ""
"You need a Google account to use Google Live.  To get one, go to http://api."
"google.com/\n"
"\n"
"When you have created your account, you should recieve a Google API key by "
"mail.  Place this key in the file\n"
"\n"
"~/.gnome2/deskbar-applet/Google.key\n"
"\n"
"If you do not receive an API key (or you have lost it) in your account "
"verification mail, then go to www.google.com/accounts and log in.  Go to api."
"google.com, click \"Create Account\" and enter your e-mail address and "
"password.  Your API key will be re-sent.\n"
"\n"
"Now download the developers kit and extract the GoogleSearch.wsdl file from "
"it.  Copy this file to\n"
"\n"
"~/.gnome2/deskbar-applet/GoogleSearch.wsdl"
msgstr ""
"Google Live-ის გამოსაყენებლად Google ანგარიში გესაჭიროებათ. მისაღებად - "
"http://api.google.com/\n"
"\n"
"თქვენი ანგარიშის შექმნის შემდეგ ელფოსტით უნდა მიიღოთ Google API კოდი.  "
"განათავსეთ იგი ფაილში\n"
"\n"
"~/.gnome2/deskbar-applet/Google.key\n"
"\n"
"If you do not receive an API key (or you have lost it) in your account "
"verification mail, then go to www.google.com/accounts and log in.  Go to api."
"google.com, click \"Create Account\" and enter your e-mail address and "
"password.  Your API key will be re-sent.\n"
"\n"
"Now download the developers kit and extract the GoogleSearch.wsdl file from "
"it.  Copy this file to\n"
"\n"
"~/.gnome2/deskbar-applet/GoogleSearch.wsdl"

#: ../deskbar/handlers/google-live.py:33
msgid "Setting Up Google Live"
msgstr "Google Live-ის გამართვა"

#: ../deskbar/handlers/google-live.py:39
msgid "You need to install the SOAPpy python module."
msgstr "გესაჭიროებათ  SOAPpy python მოდულის ჩადგმა."

#: ../deskbar/handlers/google-live.py:41
msgid "You need the Google WSDL file."
msgstr "გესაჭიროებათ  Google WSDL ფაილი."

#: ../deskbar/handlers/google-live.py:43
msgid "You need a Google API key."
msgstr "გესაჭიროებათ Google API კოდი."

#: ../deskbar/handlers/google-live.py:49
msgid "Google Search"
msgstr "Google ძიება"

#: ../deskbar/handlers/google-live.py:50
msgid "Search Google as you type"
msgstr "Google ძიება ტექსტის შეტანისას"

#: ../deskbar/handlers/history.py:10
msgid "Recognize previously used searches"
msgstr "ძინა გამოყენებული ძიებების ამოცნობა"

#: ../deskbar/handlers/iswitch-window.py:11
msgid "Window Switcher"
msgstr "ფანჯრების გადამრთველი"

#: ../deskbar/handlers/iswitch-window.py:12
msgid "Switch to an existing window by name."
msgstr "სახელის მიხედვით არსებულ ფანჯარაზე გადასვლა."

#: ../deskbar/handlers/iswitch-window.py:16
msgid "Windows"
msgstr "ფანჯრები"

#: ../deskbar/handlers/iswitch-window.py:32
#, python-format
msgid "Switch to <b>%(name)s</b>"
msgstr "გადართვა <b>%(name)s</b>"

#: ../deskbar/handlers/mozilla.py:120
msgid "You can customize which search engines are offered."
msgstr "შეგიძლიათ ძებნის ძრავების თქვენს გემოზე ამორჩევა."

#: ../deskbar/handlers/programs.py:14
msgid "Programs"
msgstr "პროგრამები"

#: ../deskbar/handlers/programs.py:15
msgid "Launch a program by its name and/or description"
msgstr "პროგრამის შესრულება მისი სახელის ანდა აღწერილობის მიხედვით"

#: ../deskbar/handlers/programs.py:19
msgid "Dictionary"
msgstr "ლექსიკონი"

#: ../deskbar/handlers/programs.py:20
msgid "Look up word definitions in the dictionary"
msgstr "სიტყვის განსაზღვრის ძებნა ლექსიკონში"

#: ../deskbar/handlers/programs.py:24
msgid "Files and Folders Search"
msgstr "ფაილებისა და დასტების ძიება"

#: ../deskbar/handlers/programs.py:25
msgid "Find files and folders by searching for a name pattern"
msgstr "ფაილებისა და დასტების ძიება სახელის თარგის მიხედვით"

#: ../deskbar/handlers/programs.py:29
msgid "Developer Documentation"
msgstr "პროგრამისტი დოკუმენტაცია"

#: ../deskbar/handlers/programs.py:30
msgid "Search Devhelp for a function name"
msgstr "Devhelp-ის ფუნქციის სახელის მიხედვით ძიება"

#. translators: First %s is the programs full name, second is the executable name
#. translators: For example: Launch Text Editor (gedit)
#: ../deskbar/handlers/programs.py:84
#, python-format
msgid "Launch <b>%(name)s</b> (%(prog)s)"
msgstr "შესრულება - <b>%(name)s</b> (%(prog)s)"

#: ../deskbar/handlers/programs.py:98
#, python-format
msgid "Lookup %s in dictionary"
msgstr "ძებნა ლექსიკონში - %s"

#: ../deskbar/handlers/programs.py:106
#, python-format
msgid "Search for file names like %s"
msgstr "%s მსგავსი სახელის ფაილების ძებნა"

#: ../deskbar/handlers/programs.py:114
#, python-format
msgid "Search in Devhelp for %s"
msgstr "%s-თვის ძიება Devhelp-ში"

#: ../deskbar/handlers/programs.py:218
#, python-format
msgid "Execute %s"
msgstr "შესრულება %s"

#: ../deskbar/handlers/web_address.py:12
msgid "Open web pages and send emails by typing a complete address"
msgstr "ვებგვერდის გახსნა ან ელბოსტის გაგზავნა სრული დასახელების მითითებით"

#: ../deskbar/handlers/web_address.py:40
#, python-format
msgid "Open the web page %s"
msgstr "ვებსაიტის გახსნა - %s"

#: ../deskbar/handlers/web_address.py:42
#, python-format
msgid "Open the location %s"
msgstr "მისამართის გახსნა - %s"

#: ../deskbar/handlers/web_address.py:58
#, python-format
msgid "Send Email to %s"
msgstr "ელფოსტის გაგზავნა - %s"

#: ../deskbar/handlers/yahoo.py:16
msgid "Yahoo! Search"
msgstr "Yahoo! ძიება"

#: ../deskbar/handlers/yahoo.py:17
msgid "Search Yahoo! as you type"
msgstr "ძიება Yahoo!-ში ტექსტის აკრეფისას"

#: ../deskbar/ui/About.py:24
msgid "An all-in-one action bar."
msgstr "ყველა-ერთში ქმედების პანელი."

#: ../deskbar/ui/About.py:27
msgid "Deskbar Website"
msgstr "Deskbar vebsaiti"

#. about.set_artists([])
#. about.set_documenters([])
#. translators: These appear in the About dialog, usual format applies.
#: ../deskbar/ui/About.py:35
msgid "translator-credits"
msgstr ""
"Malkhaz Barkalaya მალხაზ ბარკალაია <malxaz@gmail.com> Vladimer Sichinava "
"ვლადიმერ სიჭინავა <alinux@siena.linux.it>"

#: ../deskbar/ui/DeskbarPreferencesUI.py:18
msgid "New accelerator..."
msgstr "ახალი ამაჩქარებელი..."

#: ../deskbar/ui/cuemiac/DeskbarAppletButton.py:85
#: ../deskbar/ui/cuemiac/CuemiacAppletButton.py:86
msgid "Show search entry"
msgstr "საძებნი ელემენტის ჩვენება"

#: ../deskbar/ui/cuemiac/DeskbarAppletButton.py:86
#: ../deskbar/ui/cuemiac/CuemiacAppletButton.py:87
#: ../deskbar/ui/entriac/CuemiacEntryUI.py:54
msgid "Show previously used actions"
msgstr "წინა გამოყენებული ქმედებების ხილვა"

#: ../data/mozilla-search.glade.h:1
msgid "<b>Search Engines</b>"
msgstr "<b>ძიების ძრავი</b>"

#: ../data/mozilla-search.glade.h:2
msgid "Deskbar Preferences - Web Searches"
msgstr "Deskbar პარამეტრები - ვებ ძიება"

#: ../data/mozilla-search.glade.h:3
msgid "Show all available search engines"
msgstr "ანახე ყველა ხელმისაწვდომი ძიების ძრავი"

#: ../data/mozilla-search.glade.h:4
msgid "Show only the primary search engine"
msgstr "მხოლოდ პირველადი საძიებო სისტემის ჩვენება"

#: ../deskbar/ui/cuemiac/CuemiacHeader.py:16
msgid "<b>Search:</b>"
msgstr "<b>ძიება:</b>"

#: ../deskbar/ui/window/CuemiacWindowUI.py:49
msgid "_File"
msgstr "_ფაილი"

#: ../deskbar/ui/window/CuemiacWindowUI.py:50
msgid "_Quit"
msgstr "_გასვლა"

#: ../deskbar/ui/window/CuemiacWindowUI.py:51
msgid "_Edit"
msgstr "_დამუშავება"

#: ../deskbar/ui/window/CuemiacWindowUI.py:54
msgid "_View"
msgstr "_ხედი"

#: ../deskbar/ui/window/CuemiacWindowUI.py:55
msgid "_Help"
msgstr "_დახმარება"

#: ../deskbar/ui/window/CuemiacWindowUI.py:58
msgid "_History"
msgstr "_ისტორია"

#. History TreeView
#: ../deskbar/ui/window/CuemiacWindowUI.py:84
msgid "<b>History</b>"
msgstr "<b>ისტორია</b>"

