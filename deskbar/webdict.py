#
# WebDict - Freely configurable web dictionary search
#
# Copyright (c) 2010, 2012 Jan Kiszka <jan.kiszka@web.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import deskbar.interfaces.Action
import deskbar.interfaces.Match
import deskbar.interfaces.Module
from deskbar.core.GconfStore import GconfStore
from deskbar.core.Utils import load_icon
from deskbar.core.CopyToClipboardAction import CopyToClipboardAction
import gtk
import urllib
import re
import string

HANDLERS = ["WebDict"]
WEBDICT_LOG = "/tmp/webdict-deskbar.log"

def to_lower(matchobj):
    return matchobj.group(0).lower()

class WebDictAction(CopyToClipboardAction):

    def __init__(self, name, text):
        name = re.sub("%", "&#37;", name)
        name = re.sub(r'<br[^>]*>', ', ', name)
        self._formatted_name = re.sub("</?.*?>", to_lower, name)
        self._formatted_name = re.sub(r'(<[^>]*) class=".*"', r'\1', self._formatted_name)
        text = re.sub("(</?.*?>)|(&#.*?;)", "", text)
        CopyToClipboardAction.__init__(self, "", text)

    def get_verb(self):
        return self._formatted_name + "%(name)s"

class WebDictMatch(deskbar.interfaces.Match):

    def __init__(self, result, **kwargs):
        deskbar.interfaces.Match.__init__(self,
            name=result, icon="webdict-deskbar.png", **kwargs)
        self.result = result
        self.add_action(WebDictAction(self.result, self.result))

    def get_hash(self):
        return self.result

class WebDictConfig:

    BASE = GconfStore.GCONF_DIR + "/webdict/"
    QUERY_URL = BASE + "query_url"
    DIRECT_SECTION = BASE + "direct_section"
    DIRECT_MATCH = BASE + "direct_match"
    DIRECT_FORMAT = BASE + "direct_format"
    ADDON_SECTION = BASE + "addon_section"
    ADDON_MATCH = BASE + "addon_match"
    ADDON_FORMAT = BASE + "addon_format"
    MAX_LENGTH = BASE + "max_length"
    MAX_WORDS = BASE + "max_words"
    DEBUG = BASE + "debug"

    def __init__(self):
        self.conf = GconfStore.get_instance().get_client()
        self.queryURL = self.conf.get_string(WebDictConfig.QUERY_URL)
        if not self.queryURL:
            self.queryURL = ''
        self.directSection = self.conf.get_string(WebDictConfig.DIRECT_SECTION)
        if not self.directSection:
            self.directSection = ''
        self.directMatch = self.conf.get_string(WebDictConfig.DIRECT_MATCH)
        if not self.directMatch:
            self.directMatch = ''
        self.directFormat = self.conf.get_string(WebDictConfig.DIRECT_FORMAT)
        if not self.directFormat:
            self.directFormat = ''
        self.addonSection = self.conf.get_string(WebDictConfig.ADDON_SECTION)
        if not self.addonSection:
            self.addonSection = ''
        self.addonMatch = self.conf.get_string(WebDictConfig.ADDON_MATCH)
        if not self.addonMatch:
            self.addonMatch = ''
        self.addonFormat = self.conf.get_string(WebDictConfig.ADDON_FORMAT)
        if not self.addonFormat:
            self.addonFormat = ''
        self.maxLength = self.conf.get_int(WebDictConfig.MAX_LENGTH)
        self.maxWords = self.conf.get_int(WebDictConfig.MAX_WORDS)
        self.debug = self.conf.get_bool(WebDictConfig.DEBUG)

    def setQueryURL(self, expr):
        self.queryURL = expr
        self.conf.set_string(WebDictConfig.QUERY_URL, expr)

    def setDirectSection(self, expr):
        self.directSection = expr
        self.conf.set_string(WebDictConfig.DIRECT_SECTION, expr)

    def setDirectMatch(self, expr):
        self.directMatch = expr
        self.conf.set_string(WebDictConfig.DIRECT_MATCH, expr)

    def setDirectFormat(self, expr):
        self.directFormat = expr
        self.conf.set_string(WebDictConfig.DIRECT_FORMAT, expr)

    def setAddonSection(self, expr):
        self.addonSection = expr
        self.conf.set_string(WebDictConfig.ADDON_SECTION, expr)

    def setAddonMatch(self, expr):
        self.addonMatch = expr
        self.conf.set_string(WebDictConfig.ADDON_MATCH, expr)

    def setAddonFormat(self, expr):
        self.addonFormat = expr
        self.conf.set_string(WebDictConfig.ADDON_FORMAT, expr)

    def setMaxLength(self, expr):
        self.maxLength = expr
        self.conf.set_int(WebDictConfig.MAX_LENGTH, expr)

    def setMaxWords(self, expr):
        self.maxWords = expr
        self.conf.set_int(WebDictConfig.MAX_WORDS, expr)

    def setDebug(self, enabled):
        self.debug = enabled
        self.conf.set_bool(WebDictConfig.DEBUG, enabled)

class WebDictConfigDialog(gtk.Dialog):

    def __init__(self, parent, conf):
        gtk.Dialog.__init__(self, title="Configure WebDict",
                            parent=parent,
                            flags=gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                            buttons = (gtk.STOCK_OK, gtk.RESPONSE_OK))
        self.conf = conf

        tab = gtk.Table(10, 2)

        label = gtk.Label("Query URL:")
        label.set_alignment(0, 0.5)
        label.show()
        tab.attach(label, 0, 1, 0, 1, gtk.FILL, xpadding=10)
        entry = gtk.Entry()
        entry.connect("changed", self._on_queryURLChanged)
        entry.set_text(self.conf.queryURL)
        entry.set_width_chars(60)
        entry.show()
        tab.attach(entry, 1, 2, 0, 1, xpadding=10)

        label = gtk.Label("Section of direct matches:")
        label.set_alignment(0, 0.5)
        label.show()
        tab.attach(label, 0, 1, 1, 2, gtk.FILL, xpadding=10)
        entry = gtk.Entry()
        entry.connect("changed", self._on_directSectionChanged)
        entry.set_text(self.conf.directSection)
        entry.show()
        tab.attach(entry, 1, 2, 1, 2, xpadding=10)

        label = gtk.Label("Direct match:")
        label.set_alignment(0, 0.5)
        label.show()
        tab.attach(label, 0, 1, 2, 3, gtk.FILL, xpadding=10)
        entry = gtk.Entry()
        entry.connect("changed", self._on_directMatchChanged)
        entry.set_text(self.conf.directMatch)
        entry.show()
        tab.attach(entry, 1, 2, 2, 3, xpadding=10)

        label = gtk.Label("Direct match format:")
        label.set_alignment(0, 0.5)
        label.show()
        tab.attach(label, 0, 1, 3, 4, gtk.FILL, xpadding=10)
        entry = gtk.Entry()
        entry.connect("changed", self._on_directFormatChanged)
        entry.set_text(self.conf.directFormat)
        entry.show()
        tab.attach(entry, 1, 2, 3, 4, xpadding=10)

        label = gtk.Label("Section of additional matches:")
        label.set_alignment(0, 0.5)
        label.show()
        tab.attach(label, 0, 1, 4, 5, gtk.FILL, xpadding=10)
        entry = gtk.Entry()
        entry.connect("changed", self._on_addonSectionChanged)
        entry.set_text(self.conf.addonSection)
        entry.show()
        tab.attach(entry, 1, 2, 4, 5, xpadding=10)

        label = gtk.Label("Additional match:")
        label.set_alignment(0, 0.5)
        label.show()
        tab.attach(label, 0, 1, 5, 6, gtk.FILL, xpadding=10)
        entry = gtk.Entry()
        entry.connect("changed", self._on_addonMatchChanged)
        entry.set_text(self.conf.addonMatch)
        entry.show()
        tab.attach(entry, 1, 2, 5, 6, xpadding=10)

        label = gtk.Label("Additional match format:")
        label.set_alignment(0, 0.5)
        label.show()
        tab.attach(label, 0, 1, 6, 7, gtk.FILL, xpadding=10)
        entry = gtk.Entry()
        entry.connect("changed", self._on_addonFormatChanged)
        entry.set_text(self.conf.addonFormat)
        entry.show()
        tab.attach(entry, 1, 2, 6, 7, xpadding=10)

        label = gtk.Label("Maximum search length:")
        label.set_alignment(0, 0.5)
        label.show()
        tab.attach(label, 0, 1, 7, 8, gtk.FILL, xpadding=10)
        entry = gtk.Entry()
        entry.connect("changed", self._on_maxLengthChanged)
        entry.set_text("%d" % self.conf.maxLength)
        entry.show()
        tab.attach(entry, 1, 2, 7, 8, xpadding=10)

        label = gtk.Label("Maximum number of words:")
        label.set_alignment(0, 0.5)
        label.show()
        tab.attach(label, 0, 1, 8, 9, gtk.FILL, xpadding=10)
        entry = gtk.Entry()
        entry.connect("changed", self._on_maxWordsChanged)
        entry.set_text("%d" % self.conf.maxWords)
        entry.show()
        tab.attach(entry, 1, 2, 8, 9, xpadding=10)

        enableDebug = gtk.CheckButton("Enable debug log (/tmp/webdict-deskbar.log)")
        enableDebug.connect("toggled", self._on_enableDebugToggled)
        enableDebug.set_active(self.conf.debug)
        enableDebug.show()
        tab.attach(enableDebug, 0, 2, 9, 10, xpadding=10)

        tab.show()
        self.vbox.pack_start(tab, padding=10)

    def _on_queryURLChanged(self, queryURL):
        self.conf.setQueryURL(queryURL.get_text())

    def _on_directSectionChanged(self, directSection):
        self.conf.setDirectSection(directSection.get_text())

    def _on_directMatchChanged(self, directMatch):
        self.conf.setDirectMatch(directMatch.get_text())

    def _on_directFormatChanged(self, directFormat):
        self.conf.setDirectFormat(directFormat.get_text())

    def _on_addonSectionChanged(self, addonSection):
        self.conf.setAddonSection(addonSection.get_text())

    def _on_addonMatchChanged(self, addonMatch):
        self.conf.setAddonMatch(addonMatch.get_text())

    def _on_addonFormatChanged(self, addonFormat):
        self.conf.setAddonFormat(addonFormat.get_text())

    def _on_maxLengthChanged(self, maxLength):
        try:
            value = int(maxLength.get_text())
        except ValueError, e:
            value = 0;
        self.conf.setMaxLength(value)

    def _on_maxWordsChanged(self, maxWords):
        try:
            value = int(maxWords.get_text())
        except ValueError, e:
            value = 0;
        self.conf.setMaxWords(value)

    def _on_enableDebugToggled(self, enableDebug):
        self.conf.setDebug(enableDebug.get_active())

class WebDict(deskbar.interfaces.Module):

    INFOS = {'icon': load_icon("webdict-deskbar.png"),
             'name': 'WebDict',
             'description': 'Freely configurable web dictionary search',
             'version': '0.2.0',
             'categories': { "direct": { "name": "Direct dictionary matches" },
                             "addon":  { "name": "Additional dictionary matches" } },
            }

    def __init__(self):
        deskbar.interfaces.Module.__init__(self)
        self.conf = WebDictConfig()

    def show_config(self, parent):
        dialog = WebDictConfigDialog(parent, self.conf)
        dialog.run()
        dialog.destroy()

    def query(self, text):
        matches = []
        prio = 0

        if self.conf.debug:
            log = file(WEBDICT_LOG, 'w')
            log.write("Query: \'%s\'\n" % text)

        tupel = text.strip().split(' ', 1)
        if len(tupel) == 2 and '-' in tupel[0]:
            (lang, search) = tupel
        else:
            lang = "en-de"
            search = text
        lang = lang.replace('-', '')

        try:
            if self.conf.maxLength > 0 and len(search) > self.conf.maxLength:
                raise Exception("privacy filter triggered, too many characters")
            if self.conf.maxWords > 0 and len(search.split(' ')) > self.conf.maxWords:
                raise Exception("privacy filter triggered, too many words")

            url_temp = string.Template(self.conf.queryURL)
            url = url_temp.substitute(search=urllib.quote_plus(search), lang=urllib.quote_plus(lang))
            if self.conf.debug:
                log.write("URL:    \'%s\'\n\n" % url)

            htmlpage = urllib.urlopen(url)

            result = ''
            for line in htmlpage:
                result += line
            if self.conf.debug:
                log.write("***>>>*** Raw HTML page ***>>>***\n")
                log.write(result)
                log.write("***<<<*** End of raw HTML page ***<<<***\n\n")

            directSection = re.findall(self.conf.directSection, result, re.DOTALL)
            direct = ''
            for s in directSection:
                direct += s
            if self.conf.debug:
                log.write("***>>>*** Direct matches ***>>>***\n")
                log.write(direct)
                log.write("\n***<<<*** End of direct matches ***<<<***\n\n")

            directMatches = re.sub(self.conf.directMatch, self.conf.directFormat + '\n', direct)
            for line in directMatches.split('\n'):
                if line != '':
                    matches.append(WebDictMatch(line, category="direct", priority=prio))
                    prio += 1
                    if self.conf.debug:
                        log.write("Match: \'%s\'\n" % line)

            addonSection = re.findall(self.conf.addonSection, result, re.DOTALL)
            addon = ''
            for s in addonSection:
                addon += s.replace('\t','')
            if self.conf.debug:
                log.write("***>>>*** Add-on matches ***>>>***\n")
                log.write(addon)
                log.write("\n***<<<*** End of add-on matches ***<<<***\n\n")

            addonMatches = re.sub(self.conf.addonMatch, self.conf.addonFormat + '\n', addon)
            for line in addonMatches.split('\n'):
                if line != '':
                    matches.append(WebDictMatch(line, category="addon", priority=prio))
                    prio += 1
                    if self.conf.debug:
                        log.write("Match: \'%s\'\n" % line)

        except Exception, e:
            matches.append(WebDictMatch("<b>ERROR:</b> %s" % str(e)))
            if self.conf.debug:
                log.write('Exception: ' + str(e))

        self._emit_query_ready(text, matches)
