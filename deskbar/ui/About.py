# -*- coding: utf-8 -*-
from deskbar.core.Utils import launch_default_for_uri
from deskbar.defs import VERSION
from gettext import gettext as _
import gtk
import gtk.gdk


def on_email(about, mail):
    launch_default_for_uri("mailto:%s" % mail)

def on_url(about, link):
    launch_default_for_uri(link)

gtk.about_dialog_set_email_hook(on_email)
gtk.about_dialog_set_url_hook(on_url)

def show_about(parent):
    about = gtk.AboutDialog()
    infos = {
        "authors": ["Jan Kiszka <jan.kisza@web.de>"],
        "comments" : _("Web dictorary search.\nBased on Deskbar Applet."),
        "copyright" : "Copyright © 2013 Jan Kiszka.",
        "logo-icon-name" : "deskbar-applet",
        "name" : _("WebDict Deskbar"),
        "version" : VERSION,
    }

    for prop, val in infos.items():
        about.set_property(prop, val)
    
    about.connect("response", lambda self, *args: self.destroy())
    about.set_screen(parent.get_screen())
    about.show_all()
