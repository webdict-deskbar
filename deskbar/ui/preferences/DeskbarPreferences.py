import gtk
from os.path import join, basename
import deskbar
from deskbar.ui.preferences.AccelEntry import AccelEntry

class DeskbarPreferences:
    
    def __init__(self, model):
        self._model = model
    
        self.builder = gtk.Builder()
        self.builder.add_from_file(join(deskbar.SHARED_DATA_DIR, "prefs-dialog.ui"))
        
        self.dialog = self.builder.get_object("preferences")

        self.keybinding = self._model.get_keybinding()

        self.__setup_general_tab()

        self.__select_first_tab()
        
        self.sync_ui()

    def __setup_general_tab(self):
        self.keyboard_shortcut_entry = AccelEntry()
        self.keyboard_shortcut_entry.connect('accel-edited', self.on_keyboard_shortcut_entry_changed)
        self.keyboard_shortcut_entry.get_widget().show()
        self.builder.get_object("keybinding_entry_container").pack_start(self.keyboard_shortcut_entry.get_widget(), False)
        
        if self._model.get_ui_name() == deskbar.BUTTON_UI_NAME:
            spinbutton = self.builder.get_object("width")
            spinbutton.set_value(self._model.get_entry_width())
            spinbutton.connect('value-changed', self.on_entry_width_changed)
        else:
            frame = self.builder.get_object("frame_width")
            frame.hide()
        
        self.use_selection = self._model.get_use_selection()
        self.use_selection_box = self.builder.get_object("use_selection")
        self.use_selection_box.connect('toggled', self.on_use_selection_toggled)

        self.sticktopanel_checkbox = self.builder.get_object("sticktopanel_checkbox")
        self.sticktopanel_checkbox.connect("toggled", self.on_ui_changed)

        self.more_button = self.builder.get_object("more")
        self.more_button.connect("clicked", self.on_more_button_clicked)

    def __select_first_tab(self):
        """ Select first tab """
        notebook = self.builder.get_object("notebook1")
        current = notebook.get_current_page()
        if (current != 0):
            for i in range(current):
                notebook.prev_page()

    def show_run_hide(self, parent):
        self.dialog.set_screen(parent.get_screen())
        self.dialog.show()
        self.dialog.connect("response", self.on_dialog_response)

    def on_dialog_response(self, dialog, response):
        self.dialog.destroy()

    def sync_ui(self):
        if self.keybinding != None:
            self.keyboard_shortcut_entry.set_accelerator_name(self.keybinding)
        else:
            self.keyboard_shortcut_entry.set_accelerator_name("<Alt>F3")
        
        self.use_selection_box.set_active(self.use_selection)
         
        self.sticktopanel_checkbox.set_active( self._model.get_ui_name() == deskbar.BUTTON_UI_NAME)

    def on_keyboard_shortcut_entry_changed(self, entry, accel_name, keyval, mods, keycode):        
        if accel_name != "":
            self._model.set_keybinding(accel_name)
        return False

    def on_use_selection_toggled(self, toggle):
        self._model.set_use_selection(toggle.get_active())
        
    def on_more_button_clicked(self, button):
        self._model.webdict.show_config(self.dialog)

    def on_ui_changed(self, check):
        frame = self.builder.get_object("frame_width")
        if self.sticktopanel_checkbox.get_active():
            self._model.set_ui_name(deskbar.BUTTON_UI_NAME)
            frame.show()
        else:
            self._model.set_ui_name(deskbar.WINDOW_UI_NAME)
            frame.hide()

    def on_entry_width_changed(self, spinbutton):
        self._model.set_entry_width(spinbutton.get_value())
