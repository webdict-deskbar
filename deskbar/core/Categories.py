from gettext import gettext as _
from deskbar.core.Utils import load_icon

CATEGORIES = {
    # Special categories
    "default"    : {    
        "name": _("Uncategorized"),
        "icon": load_icon("unknown"),
    },
    "history" : {
        "name": _("History"),
        "icon": load_icon("document-open-recent"),
    },
}
