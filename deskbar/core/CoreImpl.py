import sys
import logging
import glib
import gtk
import deskbar
import deskbar.interfaces.Match
from deskbar.core.GconfStore import GconfStore
from deskbar.core.Keybinder import Keybinder
from deskbar.core.DeskbarHistory import DeskbarHistory
from deskbar.core.ThreadPool import ThreadPool
import deskbar.interfaces
from webdict import WebDict

LOGGER = logging.getLogger(__name__)

class CoreImpl(deskbar.interfaces.Core):
    
    DEFAULT_KEYBINDING = "<Alt>F3"
    
    def __init__(self, modules_dir):
        super(CoreImpl, self).__init__()
        
        self._loaded_modules = 0
        self._inited_modules = 0
        self._start_query_id = 0
        self._last_query = None
        self._stop_queries = True
        
        self._threadpool = ThreadPool(5)
        self._gconf = GconfStore.get_instance()
        self._history = DeskbarHistory.get_instance(self._gconf.get_max_history_items())
        self._gconf.connect("max-history-items-changed", lambda s, num: self._history.set_max_history_items(num))

        self.webdict = WebDict()
        self.webdict.connect ('query-ready', self.forward_query_ready)
        for catname, catinfo in self.webdict.INFOS["categories"].items():
            deskbar.core.Categories.CATEGORIES[catname] = catinfo

    def run(self):
        """
        Set keybinding and create L{deskbar.core.ThreadPool.ThreadPool}
        """
        self._setup_keybinder()
        self._threadpool.start()
        self._history.load()
        self._emit_initialized()
        self._keybinder.connect("activated", lambda k,t: self._emit_keybinding_activated(t))
        self._emit_loaded()

    def _setup_keybinder(self):
        self._gconf.connect("keybinding-changed", self._on_keybinding_changed)
        
        self._keybinder = Keybinder()
        keybinding = self.get_keybinding()
        if (keybinding == None or gtk.accelerator_parse(keybinding) == (0,0)):
            # Keybinding is not set or invalid, set default keybinding
            keybinding = self.DEFAULT_KEYBINDING
            self.set_keybinding(keybinding) # try to save it to Gconf
        else:
            keybinding = self.get_keybinding()
        self.bind_keybinding(keybinding) # use keybindingx

    def get_keybinding(self):
        """
        Get keybinding
        
        @return: str
        """
        return self._gconf.get_keybinding()
    
    def get_min_chars(self):
        return self._gconf.get_min_chars()
    
    def get_type_delay(self):
        return self._gconf.get_type_delay()
    
    def get_use_selection(self):
        return self._gconf.get_use_selection()

    def get_clear_entry(self):
        return self._gconf.get_clear_entry()
    
    def get_use_http_proxy(self):
        return self._gconf.get_use_http_proxy()
    
    def get_proxy_host(self):
        return self._gconf.get_proxy_host()
    
    def get_proxy_port(self):
        return self._gconf.get_proxy_port()
    
    def get_collapsed_cat(self):
        return self._gconf.get_collapsed_cat()
    
    def get_window_width(self):
        return self._gconf.get_window_width()
    
    def get_window_height(self):
        return self._gconf.get_window_height()
    
    def get_window_x(self):      
        return self._gconf.get_window_x()      
       
    def get_window_y(self):      
        return self._gconf.get_window_y()
    
    def get_hide_after_action(self):
        return self._gconf.get_hide_after_action()
    
    def get_max_history_items(self):
        return self._gconf.get_max_history_items()
    
    def get_ui_name(self):
        return self._gconf.get_ui_name()
    
    def get_entry_width(self):
        return self._gconf.get_entry_width()

    def set_keybinding(self, binding):
        """
        Store keybinding
        """
        if not self._gconf.set_keybinding(binding):
            LOGGER.error("Unable to save keybinding setting to GConf")

    def bind_keybinding(self, binding):
        """
        Actually bind keybinding
        """
        if not self._keybinder.bind(binding):
            LOGGER.error("Keybinding is already in use")
        else:
            LOGGER.info("Successfully binded Deskbar to %s", binding)
    
    def set_min_chars(self, number):
        if not self._gconf.set_min_chars(number):
            LOGGER.error("Unable to save min chars setting to GConf")
    
    def set_type_delay(self, seconds):
        if not self._gconf.set_type_delay(seconds):
            LOGGER.error("Unable to save type delay setting to GConf")
    
    def set_use_selection(self, val):
        if not self._gconf.set_use_selection(val):
            LOGGER.error("Unable to save use selection setting to GConf")

    def set_clear_entry(self, val):
        if not self._gconf.set_clear_entry(val):
            LOGGER.error("Unable to save clear entry setting to GConf")
    
    def set_use_http_proxy(self, val):
        if not self._gconf.set_use_http_proxy(val):
            LOGGER.error("Unable to save http proxy setting to GConf")
    
    def set_proxy_host(self, host):
        if not self._gconf.set_proxy_host(host):
            LOGGER.error("Unable to save http proxy host setting to GConf")
    
    def set_proxy_port(self, port):
        if not self._gconf.set_proxy_port(port):
            LOGGER.error("Unable to save proxy port setting to GConf")
    
    def set_collapsed_cat(self, cat):
        if not self._gconf.set_collapsed_cat(cat):
            LOGGER.error("Unable to save collapsed cat setting to GConf")
    
    def set_window_width(self, width):
        if not self._gconf.set_window_width(width):
            LOGGER.error("Unable to save window width setting to GConf")
    
    def set_window_height(self, height):
        if not self._gconf.set_window_height(height):
            LOGGER.error("Unable to save window height setting to GConf")
    
    def set_window_x(self, x):      
        if not self._gconf.set_window_x(x):     
            LOGGER.error("Unable to save window x position setting to GConf")
       
    def set_window_y(self, y):      
        if not self._gconf.set_window_y(y):
            LOGGER.error("Unable to save window y position setting to GConf")
    
    def set_hide_after_action(self, width):
        if not self._gconf.set_hide_after_action(width):
            LOGGER.error("Unable to save hide after action setting to GConf")
    
    def set_max_history_items(self, amount):
        if not self._gconf.set_max_history_items(amount):
            LOGGER.error("Unable to save max history items setting to GConf")
        
    def set_ui_name(self, name):
        if not self._gconf.set_ui_name(name):
            LOGGER.error("Unable to save ui name setting to GConf")
    
    def set_entry_width(self, width):
        return self._gconf.set_entry_width(width)

    def get_history(self):
        """
        @return: L{deskbar.core.DeskbarHistory.DeskbarHistory}
        """
        return self._history
    
    def stop_queries(self):
        self._stop_queries = True
        self._threadpool.stop()
    
    def query(self, text):
        """
        This method waits L{get_type_delay} milliseconds
        until the querying is started. That way we only start
        querying if search term hasn't changed for L{get_type_delay} milliseconds
        """
        if (len(text) >= self.get_min_chars()):            
            if (self._start_query_id != 0):
                glib.source_remove(self._start_query_id)
            self._start_query_id = glib.timeout_add( self.get_type_delay(), self._query_real, text )
            self._stop_queries = False
            
    def _query_real(self, text):
        self._last_query = text
        self._threadpool.callInThread(self.webdict.query, text)

    def forward_query_ready(self, handler, query, matches):
        if query == self._last_query and matches != None and not self._stop_queries:
            for match in matches:
                if not isinstance(match, deskbar.interfaces.Match):
                    raise TypeError("Handler %r returned an invalid match: %r", handler,  match)
            self._emit_query_ready(matches)

    def _on_keybinding_changed(self, store, keybinding):
        if gtk.accelerator_parse(keybinding) != (0,0):
            self.bind_keybinding(keybinding)
