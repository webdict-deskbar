#!/usr/bin/python
#
# WebDict Test wrapper
#
# Copyright (c) 2010 Jan Kiszka <jan.kiszka@web.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys

WebDictDeskbar = __import__('webdict-deskbar')
webdict = WebDictDeskbar.WebDict()

for arg in sys.argv[1:]:
    if arg[0] == '-':
        if arg == '-c':
            webdict.show_config(None)
    else:
        webdict.query(arg)
